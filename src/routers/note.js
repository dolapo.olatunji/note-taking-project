const express = require('express')
const Note = require('../models/note')
const auth = require('../middleware/auth')
const router = new express.Router()

// create notes
router.post('/v1/note', auth, async (req, res) => {
    const note = new Note({
        title: req.body.title,
        body : req.body.body,
    })
    try {
        await note.save()
        res.status(201).send(note)
    } catch (e) {
        res.status(400).send(e)
    }
})

// fetch all notes
router.get('/v1/note', auth, async(req, res) => {
    Note.find({}).then((notes) =>{
        res.send(notes)
    }).catch((e) => {
        res.status(500).send()
    })
})

// fetch a single notes
router.get('/v1/note/:id', async(req, res) => {
    const _id = req.params.id

    try {
        const note = await Note.findById(_id)

        if (!note) {
            return res.status(404).send()
        }

        res.send(note)
    } catch (e) {
        res.status(500).send()
    }
   
})

// update a note 
router.patch('/v1/notes/:id', auth, async(req, res) =>{
    const updates = Object.keys(req.body)
    const allowedUpdates = ['title', 'body']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    
    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' })
    }

    try {
        const note = await Note.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators : true})

        if (!note) {
            return res.status(404).send()
        }

        updates.forEach((update) => note[update] = req.body[update])
        await note.save()
        res.send(note)
    } catch (e) {
        res.status(400).send(e)
    }
})

// delete a note
router.delete('/v1/notes/:id', auth, async(req, res) => {
    try {
        const note = await Note.findByIdAndDelete(req.params._id)

        if (!note) {
            res.status(404).send()
        }
        res.send(note)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router