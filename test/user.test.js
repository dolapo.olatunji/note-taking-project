const request = require('supertest')
const app = require('../src/index')
const User = require('../src/models/user')
const jwt = require('jsonwebtoken')
const { userOneId, userOne, setupDatabase } = require('./fixtures/db')


test('Should register a new user', async () => {
    const response = await request(app).post('/v1/auth/register').send({
        first_name: 'Andrew',
        last_name: 'Mead',
        email: 'andrew@example.com',
        password: 'MyPass777!'
    })
})

test('Should login existing user', async () => {
    const response = await request(app).post('/v1/auth/login').send({
        email: userOne.email,
        password: userOne.password
    })
})

test('Should not login nonexistent user', async () => {
    await request(app).post('/v1/auth/login').send({
        email: userOne.email,
        password: 'thisisnotmypass'
    }).expect(400)
})