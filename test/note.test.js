const request = require('supertest')
const app = require('../src/index')
const Note = require('../src/models/note')
const {
    userOneId,
    userOne,
    userTwoId,
    userTwo,
    noteOne,
    noteTwo,
    noteThree,
    setupDatabase
} = require('./fixtures/db')

test('Should create note for user', async () => {
    const response = await request(app)
        .post('/notes')
        // .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'From my test'
        })
        // .expect(201)
    // const note = await Note.findById(response.body._id)
    // expect(note).not.toBeNull()
})

test('Should fetch user notes', async () => {
    const response = await request(app)
    .get('/notes')
    // .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    // .expect(200)
    // expect(response.body.length).toEqual(2)
})

test('Should not delete other users notes', async () => {
    const response = await request(app)
        .delete(`/notes/${noteOne._id}`)
        // .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(404)
    const note = await Note.findById(noteOne._id)
    // expect(note).not.toBeNull()
})