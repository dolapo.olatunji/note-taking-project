const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const User = require('../../src/models/user')
const Note = require('../../src/models/note')

const userOneId = new mongoose.Types.ObjectId()
const userOne = {
    _id: userOneId,
    first_name: 'Dee',
    last_name: 'Dee',
    email: 'deedee@example.com',
    password: 'what20!!',
    // tokens: [{
    //     token: jwt.sign({ _id: userOneId }, process.env.JWT_SECRET)
    // }]
}

const userTwoId = new mongoose.Types.ObjectId()
const userTwo = {
    _id: userTwoId,
    first_name: 'Jess',
    last_name : 'Gaius',
    email: 'jess@example.com',
    password: 'myhouse099@@',
    // tokens: [{
    //     token: jwt.sign({ _id: userTwoId }, process.env.JWT_SECRET)
    // }]
}

const noteOne = {
    _id: new mongoose.Types.ObjectId(),
    title:"Write Test",
    body : "it is important to write test"
}

const noteTwo = {
    _id: new mongoose.Types.ObjectId(),
    title:"Test",
    body : "is it important to write test"
}

const noteThree = {
    _id: new mongoose.Types.ObjectId(),
    title:"Write",
    body : "it is important to write"
}

const setupDatabase = async () => {
    await User.deleteMany()
    await Note.deleteMany()
    await new User(userOne).save()
    await new User(userTwo).save()
    await new Note(noteOne).save()
    await new Note(noteTwo).save()
    await new Note(noteThree).save()
}

module.exports = {
    userOneId,
    userOne,
    userTwoId,
    userTwo,
    noteOne,
    noteTwo,
    noteThree,
    setupDatabase
}